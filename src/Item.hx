package;

import com.haxepunk.Entity;
import com.haxepunk.Graphic;
import com.haxepunk.HXP;
import com.haxepunk.Mask;
import com.haxepunk.Sfx;

/**
 * ...
 * @author DDRKirby(ISQ)
 */
class Item extends Entity
{
	var _shotgunImage:SharpImage = new SharpImage("img/shotgun_item.png");
	var _bassgunImage:SharpImage = new SharpImage("img/bassgun_item.png");
	var _lasergunImage:SharpImage = new SharpImage("img/lasergun_item.png");
	var _icegunImage:SharpImage = new SharpImage("img/icegun_item.png");
	
	var _images:Array<SharpImage>;
	var _weapon:Int = 0;
	
	var _velY:Float = 0;

	public function new()
	{
		super(0, 0);
		type = "item";
		layer = 60;
		setHitbox(20, 20, 10, 20);
		
		_images = [_shotgunImage, _bassgunImage, _lasergunImage, _icegunImage];
		for (image in _images) {
			image.originX = 10;
			image.originY = 20;
			addGraphic(image);
		}
	}
	
	public function Reset(x:Float, y:Float)
	{
		this.x = x;
		this.y = y;
		_velY = 0;
		
		// Randomize weapon.
		_weapon = HXP.rand(_images.length);
		
		for (image in _images) {
			image.visible = false;
		}
		_images[_weapon].visible = true;
	}
	
	override public function update():Void 
	{
		super.update();
		
		var collidedPlayer:Entity = collide("player", x, y);
		if (collidedPlayer != null) {
			Pickup(cast(collidedPlayer, Player));
		}
	}
	
	private function Pickup(player:Player)
	{
		if (player.Dead) {
			return;
		}
		var weapon:Weapon = null;
				
		switch (_weapon) {
			case 0:
				var snare:SnareShotgun = MainScene.Instance.create(SnareShotgun);
				snare.Reset();
				weapon = snare;
			case 1:
				var snare:BassGun = MainScene.Instance.create(BassGun);
				snare.Reset();
				weapon = snare;
			case 2:
				var snare:LaserGun = MainScene.Instance.create(LaserGun);
				snare.Reset();
				snare.OwningPlayer = player;
				weapon = snare;
			case 3:
				var snare:IceGun = MainScene.Instance.create(IceGun);
				snare.Reset();
				weapon = snare;
		}
		
		player.GetNewWeapon(weapon);
		new Sfx("sfx/item" + AudioExt.Ext).play();
		
		MainScene.Instance.Score();
		
		MainScene.Instance.recycle(this);
	}
}
