package;
import com.haxepunk.Sfx;

/**
 * ...
 * @author DDRKirby(ISQ)
 */
class Laser extends PlayerBullet
{
	private var _image:SharpImage = new SharpImage("img/laser.png");
	private var _timer:Int = 0;
	
	private var _initY:Float = 0;
	private var _updown:Int;

	public function new() 
	{
		super();
		graphic = _image;
		setHitbox(6, 3, 4, 5);
		_image.originY = 5;
	}
	
	public function Reset(x:Float, y:Float, velX:Float, velY:Float, damage:Float, hits:Int, duration:Int, direction:Int, updown:Int)
	{
		_updown = updown;
		_initY = y;
		_timer = 0;
		super.BaseReset(x, y, velX, velY, damage, hits, duration);
		_image.originX = direction == -1 ? _image.width:0;
		originX = Math.round(direction == -1 ? width : 0);
		
		_image.color = _updown == -1 ? 0xFFFF00 : 0xFF00FF;
	}
	
	override public function update():Void 
	{
		super.update();
		
		y = _initY + Math.sin(_timer * 0.3) * 10 * _updown;
		_timer++;
	}
	
	override function CollideWithLevel():Void 
	{
	}
}