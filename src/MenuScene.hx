package;

import com.haxepunk.graphics.Image;
import com.haxepunk.graphics.Text;
import com.haxepunk.HXP;
import com.haxepunk.Scene;
import com.haxepunk.Sfx;
import com.haxepunk.utils.Input;
import com.haxepunk.utils.Key;

/**
 * ...
 * @author DDRKirby(ISQ)
 */
class MenuScene extends Scene
{
	var _music:Sfx = new Sfx("sfx/menu" + AudioExt.Ext);
	
	var _choices:Array<Text> = new Array<Text>();
	static var _selectedChoice:Int = 0;
	var _cursor:SharpImage = new SharpImage("img/cursor.png");
	var _fader:SharpImage = new SharpImage("img/white.png");
	
	static inline var kChoiceStartY = 170;
	static inline var kChoiceSpacingY = 10;
	static inline var kFadeoutDuration = 120;
	
	var _fadeTimer:Int = 0;
	
	override public function begin() 
	{
		super.begin();
		
		addGraphic(new Image("img/menu.png"));
		
		var title:Text = new Text("Rhythm Gunner", 250, 100);
		title.size = 24;
		title.originX = title.textWidth / 2;
		title.smooth = false;
		addGraphic(title);
		var title2:Text = new Text("by DDRKirby(ISQ)", 250, 125);
		title2.size = 8;
		title2.originX = title2.textWidth / 2;
		title2.smooth = false;
		addGraphic(title2);
		var title3:Text = new Text("Created in 48 hrs for LD32 - " + Main.kVersion, 250, 280);
		title3.size = 8;
		title3.originX = title3.textWidth / 2;
		title3.smooth = false;
		addGraphic(title3);
		
		_music.loop(Song.kMusicVolume);
		
		_choices.push(new Text("Single player mode"));
		_choices.push(new Text("Co-op mode"));
		_choices.push(new Text("Lag calibration"));
		
		for (i in 0..._choices.length) {
			_choices[i].y = kChoiceStartY + kChoiceSpacingY * i;
			_choices[i].x = 250 - 45;
			_choices[i].size = 8;
			addGraphic(_choices[i]);
			_choices[i].smooth = false;
		}
		
		_cursor.x = 250 - 55;
		addGraphic(_cursor);
		
		_fader.scale = 1100;
		_fader.color = 0x000000;
		_fader.alpha = 0;
		addGraphic(_fader);
		
		var flash:ScreenFlash = create(ScreenFlash);
		flash.Reset(0x000000, 1, 0.05);
	}
	
	override public function update() 
	{
		super.update();
		
		_cursor.y = kChoiceStartY + _selectedChoice * kChoiceSpacingY;

		if (_selectedChoice == 2) {
			_choices[2].text = "Lag calibration: <- " + Math.round(Song.LagCalibration * 1000) + "ms ->";
		} else {
			_choices[2].text = "Lag calibration:    " + Math.round(Song.LagCalibration * 1000) + "ms";
		}
		
		if (_fadeTimer > 0) {
			// we are fading, just do the fade and nothing else.
			_music.volume -= Song.kMusicVolume / kFadeoutDuration;
			_fader.alpha += 1 / kFadeoutDuration;
			
			if (_fadeTimer > kFadeoutDuration) {
				switch(_selectedChoice) {
					case 0:
						// singleplayer
						_music.stop();
						HXP.scene = new MainScene(1);
					case 1:
						// coop
						_music.stop();
						HXP.scene = new MainScene(2);
				}
				return;
			}
			
			_fadeTimer++;
			return;
		}
		
		if (Input.pressed(Key.DOWN)) {
			_selectedChoice++;
			_selectedChoice = (_selectedChoice + _choices.length) % _choices.length;
			new Sfx("sfx/cursor" + AudioExt.Ext).play();
		}
		if (Input.pressed(Key.UP)) {
			_selectedChoice--;
			_selectedChoice = (_selectedChoice + _choices.length) % _choices.length;
			new Sfx("sfx/cursor" + AudioExt.Ext).play();
		}
		
		if (Input.pressed(Key.LEFT) && _selectedChoice == 2) {
			Song.LagCalibration -= 0.01;
			Song.LagCalibration = Math.min(Song.LagCalibration, 0.5);
			Song.LagCalibration = Math.max(Song.LagCalibration, -0.5);
			new Sfx("sfx/cursor" + AudioExt.Ext).play();
		}
		if (Input.pressed(Key.RIGHT) && _selectedChoice == 2) {
			Song.LagCalibration += 0.01;
			Song.LagCalibration = Math.min(Song.LagCalibration, 0.5);
			Song.LagCalibration = Math.max(Song.LagCalibration, -0.5);
			new Sfx("sfx/cursor" + AudioExt.Ext).play();
		}
		
		if (Input.pressed(Key.ENTER) || Input.pressed(Key.SPACE)) {
			switch(_selectedChoice) {
				case 0:
					// singleplayer
					_fadeTimer++;
					new Sfx("sfx/gamestart" + AudioExt.Ext).play(0.8);
				case 1:
					// coop
					_fadeTimer++;
					new Sfx("sfx/gamestart" + AudioExt.Ext).play(0.8);
				case 2:
					
			}
		}
	}
}