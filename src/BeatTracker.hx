package;

import com.haxepunk.Entity;
import com.haxepunk.Graphic;
import com.haxepunk.graphics.Image;
import com.haxepunk.Mask;

/**
 * ...
 * @author DDRKirby(ISQ)
 */
class BeatTracker extends Entity
{
	var _beat:Float;
	var _player:Player;
	var _image:Image;

	public function new(x:Float, y:Float, beat:Float, player:Player)
	{
		_image = new SharpImage("img/play_tracker.png");
		super(x, y, _image);
		_image.centerOrigin();
		_beat = beat;
		_player = player;
		layer = -1500;
	}
	
	override public function update():Void 
	{
		super.update();
		
		visible = _player.CurrentWeapon.Beats.indexOf(_beat) != -1;
		
		if (!visible) {
			return;
		}
		
		if (MainScene.Instance.CurrentSong.BeatPosition() >= _beat && _image.scale == 1 && MainScene.Instance.CurrentSong.BeatPosition() <= _beat + 0.2) {
			_image.scale = 1.5;
		}
		
		if (_image.scale > 1) {
			_image.scale = Math.max(1, _image.scale - 0.05);
		}
		
		if (_player.CurrentWeapon.AlreadyUsedBeats.indexOf(_beat) != -1) {
			if (_player.CurrentWeapon.PoorBeats.indexOf(_beat) != -1) {
				_image.color = 0x990000;
			} else {
				_image.color = 0x00FF00;
			}
		} else {
			_image.color = 0x0080FF;
		}
	}
}
