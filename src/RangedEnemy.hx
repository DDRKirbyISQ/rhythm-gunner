package;

import com.haxepunk.Graphic;
import com.haxepunk.Sfx;

/**
 * ...
 * @author DDRKirby(ISQ)
 */
class RangedEnemy extends Enemy
{
	private static inline var kMoveSpeed:Float = 0.5;
	private static inline var kHealth:Float = 3.0;
	var _shootTimer:Int = 0;
	private static inline var kShootInterval:Int = 120;
	private static inline var kBulletSpeed:Float = 1.5;
	
	public function new()
	{
		super();
		_spritemap = new SharpSpritemap("img/ranged_enemy.png", 20, 20);
		_spritemap.originX = 10;
		_spritemap.originY = 10;
		setHitbox(16, 16, 8, 8);
		_spritemap.add("walk", [0, 1, 2, 3, 4, 5], 30);
	}
	
	public function Reset(x:Float, y:Float)
	{
		super.ResetBase(x, y, kHealth, _spritemap);
		_spritemap.play("walk");
		_shootTimer = 0;
	}
	
	override public function update():Void 
	{
		super.update();
		
		_shootTimer++;
		if (_shootTimer % kShootInterval == 0) {
			Shoot();
		}
	}
	
	private function Shoot()
	{
		var target:Player = null;
		var dist:Float = 99999;
		
		for (player in MainScene.Instance._players) {
			if (player.Dead) {
				continue;
			}
			
			if (distanceFrom(player) < dist) {
				dist = distanceFrom(player);
				target = player;
			}
		}
		
		if (target == null) {
			return;
		}
		
		var bullet:RangedEnemyBullet = MainScene.Instance.create(RangedEnemyBullet);
		var deltaX = target.x - x;
		var deltaY = target.y - y;
		
		new Sfx("sfx/ranged_enemy" + AudioExt.Ext).play();
		
		bullet.BaseReset(x, y, deltaX / distanceFrom(target) * kBulletSpeed, deltaY / distanceFrom(target) * kBulletSpeed, 240);
	}
	
	override function HandleMovement()
	{
		var target:Player = null;
		var dist:Float = 99999;
		
		for (player in MainScene.Instance._players) {
			if (player.Dead) {
				continue;
			}
			
			if (distanceFrom(player) < dist) {
				dist = distanceFrom(player);
				target = player;
			}
		}
		
		if (target == null) {
			return;
		}
		
		moveTowards(target.x, target.y, kMoveSpeed);
		
	}
	
	override function HandleVerticalMovement() 
	{
		
	}
}
