package;

import com.haxepunk.Graphic.ImageType;
import com.haxepunk.graphics.Image;
import flash.geom.Rectangle;

/**
 * ...
 * @author DDRKirby(ISQ)
 */
class SharpImage extends Image
{

	public function new(?source:ImageType, ?clipRect:Rectangle) 
	{
		super(source, clipRect);
		smooth = false;
	}
	
}