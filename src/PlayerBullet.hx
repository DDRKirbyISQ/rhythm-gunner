package;

import com.haxepunk.Entity;
import com.haxepunk.Graphic;
import com.haxepunk.HXP;
import com.haxepunk.Mask;

/**
 * ...
 * @author DDRKirby(ISQ)
 */
class PlayerBullet extends Entity
{
	private var _velX:Float;
	private var _velY:Float;
	private var _hits:Int;
	private var _damage:Float;
	private var _duration:Int;

	public function new()
	{
		super();
		type = "playerbullet";
		layer = -100;
	}
	
	public function BaseReset(x:Float, y:Float, velX:Float, velY:Float, damage:Float, hits:Int, duration:Int)
	{
		this.graphic = graphic;
		this.x = x;
		this.y = y;
		_velX = velX;
		_velY = velY;
		_damage = damage;
		_hits = hits;
		_duration = duration;
	}
	
	override public function update():Void 
	{
		super.update();
		
		Move();
		
		HandleDuration();
		
		if (collide("level", x, y) != null) {
			CollideWithLevel();
		}
		
		var collidedEnemy:Entity = collide("enemy", x, y);
		if (collidedEnemy != null) {
			CollideWithEnemy(cast(collidedEnemy, Enemy));
		}
	}
	
	private function HandleDuration()
	{
		_duration--;
		if (_duration == 0) {
			MainScene.Instance.recycle(this);
		}
	}
	
	private function Move()
	{
		x += _velX;
		y += _velY;
	}
	
	private function CollideWithLevel():Void
	{
		MainScene.Instance.recycle(this);
	}
	
	private function CollideWithEnemy(enemy:Enemy):Void
	{
		enemy.Damage(_damage);
		_hits--;
		if (_hits <= 0) {
			MainScene.Instance.recycle(this);
		}
	}
}