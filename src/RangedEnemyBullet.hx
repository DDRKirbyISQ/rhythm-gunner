package;
import com.haxepunk.graphics.Image;

/**
 * ...
 * @author DDRKirby(ISQ)
 */
class RangedEnemyBullet extends EnemyBullet
{
	private var _image:SharpImage = new SharpImage("img/bullet.png");
	
	public function new() 
	{
		super();
		graphic = _image;
		setHitbox(6, 6, 3, 3);
		_image.centerOrigin();
	}
	
	
	override function CollideWithLevel():Void 
	{
	}
}