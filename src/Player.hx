package;

import com.haxepunk.Entity;
import com.haxepunk.Graphic;
import com.haxepunk.graphics.Spritemap;
import com.haxepunk.HXP;
import com.haxepunk.Mask;
import com.haxepunk.Sfx;
import com.haxepunk.utils.Input;

/**
 * ...
 * @author DDRKirby(ISQ)
 */
class Player extends Entity
{
	private static inline var kMoveSpeed:Float = 2.0;
	private static inline var kGravity:Float = 0.5;
	private static inline var kGravityJumping:Float = 0.25;
	private static inline var kGravityDead:Float = 0.1;
	private static inline var kJumpVel:Float = -4.9;
	private static inline var kMoveIncrement:Float = 0.1;
	private static inline var kWeaponAttachX = -10;
	private static inline var kWeaponAttachY = -20;

	private var _spritemap:SharpSpritemap = new SharpSpritemap("img/player.png", 20, 20);
	private var _velY:Float = 0.0;
	private var _playerNumber:Int;
	public var Dead:Bool = false;
	public var CurrentWeapon:Weapon = null;
	private var _dieVelX:Float = 0.0;
	private var _beatCircles:Array<BeatCircle> = new Array<BeatCircle>();

	public function new(x:Float, y:Float, playerNumber:Int)
	{
		super(x, y, _spritemap);
		setHitbox(14, 16, 7, 16);
		_spritemap.originX = 10;
		_spritemap.originY = 20;
		layer = 0;
		type = "player";
		_playerNumber = playerNumber;
		
		var shotgun:SnareShotgun = MainScene.Instance.create(SnareShotgun);
		shotgun.Reset();
		GetNewWeapon(shotgun);
		
		if (_playerNumber == 0) {
			_spritemap.add("idle", [0]);
			_spritemap.play("idle");
			_spritemap.add("bob", [1]);
			_spritemap.add("walk", [2, 3, 5, 6], 10);
			_spritemap.add("jump", [6]);
			_spritemap.add("dead", [8]);
		} else if (_playerNumber == 1) {
			_spritemap.add("idle", [10]);
			_spritemap.play("idle");
			_spritemap.add("bob", [11]);
			_spritemap.add("walk", [12, 13, 15, 16], 10);
			_spritemap.add("jump", [17]);
			_spritemap.add("dead", [18]);
		}
	}
	
	public function UnDie()
	{
		var shotgun:SnareShotgun = MainScene.Instance.create(SnareShotgun);
		shotgun.Reset();
		GetNewWeapon(shotgun);
		Dead = false;
		_spritemap.play("idle");
		_velY = 0;
		_dieVelX = 0;
	}
	
	override public function update():Void 
	{
		super.update();

		// Handle input.
		HandleInput();
		
		// Handle vertical movement.
		HandleVerticalMovement();
		HandleHorizontalMovement();
		
		// Handle attached weapon.
		if (CurrentWeapon != null) {
			CurrentWeapon.x = x + kWeaponAttachX * Direction();
			CurrentWeapon.y = y + kWeaponAttachY;
			CurrentWeapon.ThisSpritemap.flipped = _spritemap.flipped;
			CurrentWeapon.UpdateOrigin();
		}
		
		// Handle bobbing.
		HandleBobbing();

		// Track beat circles.trace
		for (beatCircle in _beatCircles) {
			beatCircle.x = x;
			beatCircle.y = y - 10;
		}
	}

	// Should be called after a new weapon is gotten.
	private function GenerateBeatCircles()
	{
		for (beatcircle in _beatCircles) {
			MainScene.Instance.recycle(beatcircle);
		}
		_beatCircles = new Array<BeatCircle>();

		for (beat in CurrentWeapon.Beats) {
			var beatCircle:BeatCircle = MainScene.Instance.create(BeatCircle);
			beatCircle.Reset(this, beat);
			_beatCircles.push(beatCircle);
		}
	}
	
	private function HandleBobbing():Void
	{
		var temp = MainScene.Instance.CurrentSong.BeatDiff([0, 1, 2, 3, 4]);

		if (_spritemap.currentAnim == "idle") {
			if (temp.result < 0.05) {
				_spritemap.play("bob");
			}
		} else if (_spritemap.currentAnim == "bob") {
			if (temp.result > 0.05) {
				_spritemap.play("idle");
			}
		}
	}
	
	private function HandleVerticalMovement()
	{
		if (Dead) {
			_velY += kGravityDead;
		} else if (Input.check('player${_playerNumber}_jump')) {
			_velY += kGravityJumping;
		} else {
			_velY += kGravity;
		}

		if (!TryMoveYIncrement(_velY)) {
			if (Dead) {
				if (_velY > 0) {
					_velY = 0.0;
					_dieVelX = 0.0;
				}
			} else {
				_velY = 0.0;
			}
		}
	}
	
	private function HandleHorizontalMovement()
	{
		if (!TryMoveXIncrement(_dieVelX)) {
			_dieVelX = 0.0;
		}
	}
	public function HandleInput()
	{
		if (Dead) {
			return;
		}
		
		if (MainScene.Instance._players.length > 1 && MainScene._coopProgress == 0) {
			return;
		}
		
		if (MainScene.Instance._players.length == 1 && (MainScene._tutorialProgress == 0 || MainScene._tutorialProgress == 2)) {
			return;
		}
		
		if (Input.pressed('player${_playerNumber}_jump')) {
			TryJump();
		}
		
		if (Input.check('player${_playerNumber}_left') && !Input.check('player${_playerNumber}_right')) {
			TryLeft();
		} else if (Input.check('player${_playerNumber}_right') && !Input.check('player${_playerNumber}_left')) {
			TryRight();
		} else {
			// Standing or falling
			if (IsGrounded() && _velY == 0.0) {
				_spritemap.play("idle");
			}
		}
		
		if (Input.pressed('player${_playerNumber}_shoot')) {
			TryShoot();
		}
	}
	
	public function Direction():Int
	{
		return _spritemap.flipped ? -1 : 1;
	}
	
	public function TryLeft()
	{
		if (MainScene.Instance._players.length == 1 && MainScene._tutorialProgress < 5 && MainScene._tutorialProgress != 1) {
			return;
		}
		_spritemap.flipped = true;
		_spritemap.updateBuffer();
		if (TryMoveXIncrement( -kMoveSpeed) && IsGrounded() && _velY == 0.0) {
			_spritemap.play("walk");
		}
		
	}
	
	public function TryRight()
	{
		if (MainScene.Instance._players.length == 1 && MainScene._tutorialProgress < 5 && MainScene._tutorialProgress != 1) {
			return;
		}
		_spritemap.flipped = false;
		_spritemap.updateBuffer();
		if (TryMoveXIncrement(kMoveSpeed) && IsGrounded() && _velY == 0.0) {
			_spritemap.play("walk");
		}
	}
	
	public function TryJump()
	{
		if (MainScene.Instance._players.length == 1 && MainScene._tutorialProgress < 5 && MainScene._tutorialProgress != 1) {
			return;
		}
		
		if (!IsGrounded()) {
			return;
		}
		
		_velY = kJumpVel;
		_spritemap.play("jump");
		new Sfx("sfx/jump" + AudioExt.Ext).play();
	}
	
	public function TryShoot()
	{
		if (MainScene.Instance._players.length == 1 && MainScene._tutorialProgress == 1) {
			return;
		}
		
		
		if (CurrentWeapon != null) {
			CurrentWeapon.TryFire(Direction());
		}
	}
	
	public function GetNewWeapon(weapon:Weapon)
	{
		// Remove old weapon first.
		if (CurrentWeapon != null) {
			HXP.scene.recycle(CurrentWeapon);
		}
		
		// Now assign new weapon.
		CurrentWeapon = weapon;
		CurrentWeapon.x = x;
		CurrentWeapon.y = y;

		// Generate beat circles.
		GenerateBeatCircles();
	}
	
	public function Hit()
	{
		if (Dead) {
			return;
		}
		
		_spritemap.play("dead");
		new Sfx("sfx/death" + AudioExt.Ext).play(1.5);
		Dead = true;
		_velY = -2;
		_dieVelX = _spritemap.flipped ? 1.5 : -1.5;
		MainScene.Instance.Shake(10);
		var flash:ScreenFlash = MainScene.Instance.create(ScreenFlash);
		flash.Reset(0xFFFFFF, 0.5, 0.025);
	}
	
	private function IsGrounded()
	{
		return collide("level", x, y + 2) != null;
	}
	
	private function TryMoveXIncrement(amount:Float):Bool
	{
		var increment:Float = amount > 0 ? kMoveIncrement : -kMoveIncrement;
		var progress:Float = 0;
		
		while (Math.abs(progress) < Math.abs(amount)) {
			if (Math.abs(progress) + Math.abs(increment) < Math.abs(amount)) {
				if (!TryMoveX(increment)) {
					return false;
				}
				progress += increment;
			} else {
				return TryMoveX(amount - progress);
			}
		}
		return true;
	}
	
	private function TryMoveX(amount:Float):Bool
	{
		if (collide("level", x + amount, y) == null) {
			x += amount;
			return true;
		} else {
			return false;
		}
	}
	
	private function TryMoveYIncrement(amount:Float):Bool
	{
		var increment:Float = amount > 0 ? kMoveIncrement : -kMoveIncrement;
		var progress:Float = 0;
		
		while (Math.abs(progress) < Math.abs(amount)) {
			if (Math.abs(progress) + Math.abs(increment) < Math.abs(amount)) {
				if (!TryMoveY(increment)) {
					return false;
				}
				progress += increment;
			} else {
				return TryMoveY(amount - progress);
			}
		}
		return true;
	}
	
	private function TryMoveY(amount:Float):Bool
	{
		if (collide("level", x, y + amount) == null) {
			y += amount;
			return true;
		} else {
			return false;
		}
	}
}
