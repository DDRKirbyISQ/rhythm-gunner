package;
import com.haxepunk.Entity;
import com.haxepunk.graphics.Image;
import com.haxepunk.graphics.Text;

/**
 * ...
 * @author DDRKirby(ISQ)
 */
class WeaponText extends Entity
{
	private var _player:Player;
	private var _text:Text = new Text("");

	public function new(x:Float, y:Float, player:Player)
	{
		_player = player;
		UpdateText();
		
		super(x, y, _text);
		_text.size = 8;
		_text.smooth = false;
	}
	
	override public function update():Void 
	{
		super.update();
		
		UpdateText();
	}
	
	private function UpdateText()
	{
		if (Std.is(_player.CurrentWeapon, SnareShotgun)) {
			_text.text = "Snare Shotgun";
		}
		if (Std.is(_player.CurrentWeapon, BassGun)) {
			_text.text = "BASS Gun";
		}
		if (Std.is(_player.CurrentWeapon, LaserGun)) {
			_text.text = "Vibrato Laser";
		}
		if (Std.is(_player.CurrentWeapon, IceGun)) {
			_text.text = "Echo Ice";
		}
		
		_text.originX = _text.textWidth / 2;
	}
}
