package;

import com.haxepunk.Graphic;
import com.haxepunk.graphics.Spritemap;
import com.haxepunk.HXP;
import com.haxepunk.Mask;
import com.haxepunk.Sfx;

/**
 * ...
 * @author DDRKirby(ISQ)
 */
class SnareShotgun extends Weapon
{
	static inline var kFireX = 35;
	static inline var kFireY = 12;
	static inline var kSnareX = 15;
	static inline var kSnareY = 15;
	
	public function new()
	{
		super(new SharpSpritemap("img/shotgun.png", 40, 40));
		layer = -10;
		type = "snare";
		
		ThisSpritemap.add("idle", [0]);
		ThisSpritemap.add("fire", [1, 2, 0], 20, false);
	}
	
	public function Reset()
	{
		super.BaseReset();
		Beats = [1, 3];
		ThisSpritemap.play("idle");
	}
	
	override public function update():Void 
	{
		super.update();
		if (ThisSpritemap.currentAnim == "fire" && ThisSpritemap.complete) {
			ThisSpritemap.play("idle");
		}
	}
	
	override public function FireGreat(direction:Int)
	{
		_recoil = 5;
		var fireX:Float = x + kFireX * direction;
		var fireY:Float = y + kFireY;
		super.FireGreat(direction);
		var bullet:SnareShotgunBullet = MainScene.Instance.create(SnareShotgunBullet);
		bullet.Reset(fireX, fireY, 0, 0, 1, 50, 18, direction);
		MainScene.Instance.Shake(4);
		new Sfx("sfx/shotgun" + AudioExt.Ext).play(0.8);
		MainScene.Instance.GreatShine(fireX, fireY);
		ThisSpritemap.play("fire", true);
		
		var flash:ScreenFlash = MainScene.Instance.create(ScreenFlash);
		flash.Reset(0xFFFFFF, 0.2, 0.02);
		
		for (i in 0...30) {
			MainScene.Instance.MainEmitter.emit("snaredust", x + kSnareX * direction, y + kSnareY);
		}
	}		
	
	override public function FireGood(direction:Int) 
	{
		_recoil = 5;
		var fireX:Float = x + kFireX * direction;
		var fireY:Float = y + kFireY;
		super.FireGood(direction);
		var bullet:SnareShotgunBullet = MainScene.Instance.create(SnareShotgunBullet);
		bullet.Reset(fireX, fireY, 0, 0, 1, 50, 18, direction);
		ThisSpritemap.play("fire", true);

		new Sfx("sfx/shotgun" + AudioExt.Ext).play(0.8);

		var flash:ScreenFlash = MainScene.Instance.create(ScreenFlash);
		flash.Reset(0xFFFFFF, 0.2, 0.02);

		for (i in 0...20) {
			MainScene.Instance.MainEmitter.emit("snaredust", x + kSnareX * direction, y + kSnareY);
		}
	}
}
