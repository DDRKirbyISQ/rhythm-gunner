package;
import com.haxepunk.Entity;
import com.haxepunk.HXP;
import com.haxepunk.Sfx;

/**
 * ...
 * @author DDRKirby(ISQ)
 */
class Song
{
	public static var kMusicVolume:Float = 0.8;
	
	
	var _loopMap:Map<String, Sfx> = new Map<String, Sfx>();
	var _bpm:Float = 135.0;
	
	public static var LagCalibration:Float = 0.01;
	
	public function new() 
	{
		_loopMap.set("main", new Sfx("sfx/song" + AudioExt.Ext));
		_loopMap.set("snare", new Sfx("sfx/song_snare" + AudioExt.Ext));
		_loopMap.set("bass", new Sfx("sfx/song_bass" + AudioExt.Ext));
		_loopMap.set("laser", new Sfx("sfx/song_laser" + AudioExt.Ext));
		_loopMap.set("ice", new Sfx("sfx/song_ice" + AudioExt.Ext));
		_loopMap.set("!snare", new Sfx("sfx/song_nosnare" + AudioExt.Ext));
		_loopMap.set("!bass", new Sfx("sfx/song_nobass" + AudioExt.Ext));
	}
	
	public function Start()
	{
		for (key in _loopMap.keys()) {
			if (key == "main") {
				_loopMap[key].loop(kMusicVolume);
			} else {
				_loopMap[key].loop(0);
			}
		}
	}
	
	public function Stop()
	{
		for (key in _loopMap.keys()) {
			_loopMap[key].stop();
		}
	}
	public function FadeUpdate(duration:Float)
	{
		for (key in _loopMap.keys()) {
			_loopMap[key].volume -= kMusicVolume / duration;
		}
	}
	
	public function Update()
	{
		for (key in _loopMap.keys()) {
			if (key == "main") {
				continue;
			}

			if (key.charAt(0) == "!") {
				// reverse logic.
				var newkey:String = key.substr(1);
				var temp:Array<Entity> = new Array<Entity>();
				MainScene.Instance.getType(newkey, temp);
				
				
				if (temp.length == 0) {
					_loopMap[key].volume = _loopMap["main"].volume;
				} else {
					_loopMap[key].volume = 0;
				}
			} else {
				var temp:Array<Entity> = new Array<Entity>();
				MainScene.Instance.getType(key, temp);
				
				if (temp.length == 0) {
					_loopMap[key].volume = 0;
				} else {
					_loopMap[key].volume = _loopMap["main"].volume;
				}
				
			}
		}
	}
	
	public function BeatsToSeconds(beats:Float):Float
	{
		return beats / _bpm * 60.0;
	}
	public function SecondsToBeats(seconds:Float):Float
	{
		return seconds * _bpm / 60.0;
	}
	
	public function Activate(channel:String)
	{
		_loopMap[channel].volume = kMusicVolume;
	}
	
	public function Deactivate(channel:String)
	{
		_loopMap[channel].volume = 0;
	}
	
	public function CurrentTime():Float
	{
		// TODO: Compensate for lag?
		return _loopMap["main"].position - LagCalibration;
	}
	
	public function CurrentBeat():Float
	{
		return SecondsToBeats(CurrentTime());
	}
	
	public function BeatPosition():Float
	{
		return CurrentBeat() % 4.0;
	}
	
	public function BeatDiff(beats:Array<Float>):{result:Float, beatResult:Float}
	{
		var result:Float = 999999;
		var beatResult:Float = 9999;
		for (beat in beats) {
			if (Math.abs(BeatPosition() - beat) < result) {
				result = Math.abs(BeatPosition() - beat);
				beatResult = beat;
			}
			if (Math.abs(beat - BeatPosition()) < result) {
				result = Math.abs(beat - BeatPosition());
				beatResult = beat;
			}
		}
		
		return {result: result, beatResult: beatResult};
	}
}