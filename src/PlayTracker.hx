package;

import com.haxepunk.Entity;
import com.haxepunk.Graphic;
import com.haxepunk.graphics.Image;
import com.haxepunk.Mask;

/**
 * ...
 * @author DDRKirby(ISQ)
 */
class PlayTracker extends Entity
{
	private var _initialY:Int;
	private var _height:Int;
	private var _image:SharpImage = new SharpImage("img/play_tracker.png");

	public function new(x:Int, y:Int, height:Int)
	{
		super(x, y, _image);
		_image.centerOrigin();
		_image.color = 0xFFFF00;
		_initialY = y;
		_height = height;
		layer = -2000;
	}
	
	override public function update():Void 
	{
		super.update();
		
		y = _initialY + _height * MainScene.Instance.CurrentSong.BeatPosition() / 4.0;
	}
}
