package;

import com.haxepunk.Entity;
import com.haxepunk.Graphic;
import com.haxepunk.Mask;

/**
 * ...
 * @author DDRKirby(ISQ)
 */
class Explosion extends Entity
{
	var _image:SharpImage = new SharpImage("img/explosion.png");
	var _timer:Int = 0;
	var _duration:Int = 0;
	var _damage:Float = 0;

	public function new() 
	{
		super(0, 0, _image);
		_image.centerOrigin();
		setHitbox(_image.width, _image.height, 20, 20);
	}
	
	public function Reset(x:Float, y:Float, damage:Float, duration:Int)
	{
		_image.angle = 0;
		this.x = x;
		this.y = y;
		_damage = damage;
		_duration = duration;
		_timer = 0;
	}
	
	override public function update():Void 
	{
		super.update();
		
		_image.angle += 40;
		
		
		var enemies:Array<Entity> = new Array<Entity>();
		collideInto("enemy", x, y, enemies);
		for (enemy in enemies) {
			cast(enemy, Enemy).Damage(_damage);
		}
		
		_timer++;
		if (_timer > _duration) {
			MainScene.Instance.recycle(this);
			return;
		}
	}
	
}