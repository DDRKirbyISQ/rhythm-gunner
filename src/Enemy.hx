package;

import com.haxepunk.Entity;
import com.haxepunk.Graphic;
import com.haxepunk.graphics.Spritemap;
import com.haxepunk.HXP;
import com.haxepunk.Mask;

/**
 * ...
 * @author DDRKirby(ISQ)
 */
class Enemy extends Entity
{
	private var _health:Float;
	private var _velY:Float;
	
	private static inline var kMoveIncrement:Float = 0.5;
	private static inline var kGravity:Float = 0.4;
	private static inline var kDamageCounter:Int = 2;
	
	private var _spritemap:Spritemap;
	
	private var _damageCounter:Int = -1;
	
	public function ResetBase(x:Float, y:Float, health:Float, spritemap:Spritemap)
	{
		this.x = x;
		this.y = y;
		_health = health;
		_velY = 0.0;
		type = "enemy";
		layer = 50;
		_spritemap = spritemap;
		graphic = spritemap;
		_damageCounter = -1;
		_spritemap.blend = flash.display.BlendMode.NORMAL;
	}
	
	public function Damage(amount:Float)
	{
		_health -= amount;
		if (_health <= 0.0) {
			Die();
		}
		_damageCounter = 0;
	}
	
	override public function update():Void 
	{
		super.update();
		
		HandleMovement();
		HandleVerticalMovement();
		HandleShooting();
		
		var collidedPlayers:Array<Entity> = new Array<Entity>();
		collideInto("player", x, y, collidedPlayers);
		for (collidedPlayer in collidedPlayers) {
			HandlePlayerCollision(cast(collidedPlayer, Player));
		}
		
		if (_damageCounter >= 0) {
			_damageCounter++;
			if (_damageCounter == 1) {
				_spritemap.blend = flash.display.BlendMode.INVERT;
			} else if (_damageCounter == 2) {
				_spritemap.blend = flash.display.BlendMode.INVERT;
			} else if (_damageCounter == 3) {
				_spritemap.blend = flash.display.BlendMode.INVERT;
			} else {
				_spritemap.blend = flash.display.BlendMode.NORMAL;
			}
		}
	}
	
	private function HandlePlayerCollision(player:Player)
	{
		player.Hit();
	}
	
	public function Die()
	{
		MainScene.Instance.recycle(this);
	}

	private function HandleMovement()
	{
		
	}
	
	private function HandleShooting()
	{
		
	}
	
	private function HandleVerticalMovement()
	{
		_velY += kGravity;
		if (!TryMoveYIncrement(_velY)) {
			_velY = 0.0;
		}
	}
	
	private function TryMoveXIncrement(amount:Float):Bool
	{
		var increment:Float = amount > 0 ? kMoveIncrement : -kMoveIncrement;
		var progress:Float = 0;
		
		while (Math.abs(progress) < Math.abs(amount)) {
			if (Math.abs(progress) + Math.abs(increment) < Math.abs(amount)) {
				if (!TryMoveX(increment)) {
					return false;
				}
				progress += increment;
			} else {
				return TryMoveX(amount - progress);
			}
		}
		return true;
	}
	
	private function TryMoveX(amount:Float):Bool
	{
		if (collide("level", x + amount, y) == null) {
			x += amount;
			return true;
		} else {
			return false;
		}
	}
	
	private function TryMoveYIncrement(amount:Float):Bool
	{
		var increment:Float = amount > 0 ? kMoveIncrement : -kMoveIncrement;
		var progress:Float = 0;
		
		while (Math.abs(progress) < Math.abs(amount)) {
			if (Math.abs(progress) + Math.abs(increment) < Math.abs(amount)) {
				if (!TryMoveY(increment)) {
					return false;
				}
				progress += increment;
			} else {
				return TryMoveY(amount - progress);
			}
		}
		return true;
	}
	
	private function TryMoveY(amount:Float):Bool
	{
		if (collide("level", x, y + amount) == null) {
			y += amount;
			return true;
		} else {
			return false;
		}
	}
}
