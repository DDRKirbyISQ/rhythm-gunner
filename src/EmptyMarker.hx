package;

import com.haxepunk.Entity;
import com.haxepunk.Graphic;
import com.haxepunk.graphics.Image;
import com.haxepunk.Mask;

/**
 * ...
 * @author DDRKirby(ISQ)
 */
class EmptyMarker extends Entity
{
	var _beat:Float;
	var _image:SharpImage;
	var _player:Player;

	public function new(x:Float, y:Float, beat:Float, player:Player)
	{
		if (beat % 1.0 == 0) {
			_image = new SharpImage("img/empty_marker.png");
		} else {
			_image = new SharpImage("img/empty_marker_small.png");
		}
		_image.centerOrigin();
		super(x, y, _image);
		_beat = beat;
		_player = player;
		layer = -1400;
		
	}
	
	override public function update():Void 
	{
		super.update();
	}
}
