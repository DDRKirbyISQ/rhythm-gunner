package;

import com.haxepunk.Graphic;
import com.haxepunk.graphics.Spritemap;
import com.haxepunk.HXP;
import com.haxepunk.Mask;
import com.haxepunk.Sfx;

/**
 * ...
 * @author DDRKirby(ISQ)
 */
class LaserGun extends Weapon
{
	static inline var kFireX = 15;
	static inline var kFireY = 16;
	private var _fireTimer:Int = 0;
	
	public var OwningPlayer:Player;
	
	public function new()
	{
		super(new SharpSpritemap("img/lasergun.png", 40, 40));
		layer = -10;
		type = "laser";
		
		ThisSpritemap.add("idle", [0], 10);
	}
	
	public function Reset()
	{
		super.BaseReset();
		Beats = [0, 1.5, 3, 4];
		ThisSpritemap.play("idle");
		_fireTimer = 0;
	}
	
	override public function update():Void 
	{
		super.update();
		
		_fireTimer--;
		if (_fireTimer > 0) {
			// fire.
			var fireX:Float = x + kFireX * OwningPlayer.Direction();
			var fireY:Float = y + kFireY;
			var bullet:Laser = MainScene.Instance.create(Laser);
			bullet.Reset(fireX, fireY, 6 * OwningPlayer.Direction(), 0, 0.25, 1, 120, OwningPlayer.Direction(), 1);
			var bullet2:Laser = MainScene.Instance.create(Laser);
			bullet2.Reset(fireX, fireY, 6 * OwningPlayer.Direction(), 0, 0.25, 1, 120, OwningPlayer.Direction(), -1);
		}
	}
	
	override public function FireGreat(direction:Int)
	{
		_recoil = 5;
		var fireX:Float = x + kFireX * direction;
		var fireY:Float = y + kFireY;
		super.FireGreat(direction);
		new Sfx("sfx/laser" + AudioExt.Ext).play();
		MainScene.Instance.GreatShine(fireX, fireY);

		_fireTimer = 35;
	}		
	
	override public function FireGood(direction:Int) 
	{
		_recoil = 5;
		var fireX:Float = x + kFireX * direction;
		var fireY:Float = y + kFireY;
		super.FireGood(direction);
		new Sfx("sfx/laser" + AudioExt.Ext).play();
		
		_fireTimer = 20;
	}
}
