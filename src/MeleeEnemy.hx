package;

import com.haxepunk.Graphic;

/**
 * ...
 * @author DDRKirby(ISQ)
 */
class MeleeEnemy extends Enemy
{
	private static inline var kMoveSpeed:Float = 1.5;
	private static inline var kHealth:Float = 3.0;
	
	private var _direction:Int;
	
	public function new()
	{
		super();
		_spritemap = new SharpSpritemap("img/melee_enemy.png", 22, 22);
		_spritemap.originX = 11;
		_spritemap.originY = 22;
		setHitbox(22, 22, 11, 22);
		_spritemap.add("walk", [0, 1, 2, 3], 15);
	}
	
	public function Reset(x:Float, y:Float, direction:Int) 
	{
		super.ResetBase(x, y, kHealth, _spritemap);
		_direction = direction;
		_spritemap.play("walk");
	}
	
	override function HandleMovement()
	{
		
		if (y >= 250 && x >= 405) {
			x = 92;
			y = 80;
			_direction = 1;
		}

		if (y >= 250 && x <= 95) {
			x = 408;
			y = 80;
			_direction = -1;
		}
		
		if (!TryMoveXIncrement(kMoveSpeed * _direction)) {
			_direction = -_direction;
		}
		
		_spritemap.flipped = _direction < 0;
	}
}
