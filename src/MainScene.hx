import com.haxepunk.Entity;
import com.haxepunk.graphics.Emitter;
import com.haxepunk.graphics.Text;
import com.haxepunk.HXP;
import com.haxepunk.Scene;
import com.haxepunk.Sfx;
import com.haxepunk.utils.Input;
import com.haxepunk.utils.Key;

class MainScene extends Scene
{
	public static var Instance:MainScene;
	
	private var _level:Level;

	private var _enemyCounter:Int = 0;
	public var CurrentSong:Song;
	
	public var MainEmitter:Emitter = new Emitter("img/particles.png", 32, 32);
	
	public static inline var kEnemySpawnInterval = 150;
	public static inline var kEnemySpawnIntervalCoop = 100;
	
	private var _shake:Int = 0;
	
	private var _numPlayers:Int;
	public var _players:Array<Player> = new Array<Player>();
	
	private var _gameOverText:Text = new Text("GAME OVER", 250, 100);
	private var _continueText:Text = new Text("ENTER to restart", 250, 150);
	private var _continueText2:Text = new Text("ESC to quit", 250, 170);
	private var _scoreText:Text = new Text("Score: 10", 250, 270);
	
	private var _tutorialfader:SharpImage = new SharpImage("img/white.png");
	
	private var _fadeTimer:Int = 0;
	private var _fader:SharpImage = new SharpImage("img/white.png");
	
	private static inline var kFadeoutDuration:Int = 120;
	
	private var _score:Int = 0;
	
	private var _p0ui:SharpImage = new SharpImage("img/p1ui.png");
	private var _p1ui:SharpImage = new SharpImage("img/p2ui.png");
	
	private var _firstItem:Item;
	
	private var _tutorial0:SharpImage = new SharpImage("img/tutorial0.png");
	private var _tutorial1:SharpImage = new SharpImage("img/tutorial1.png");
	private var _tutorial2:SharpImage = new SharpImage("img/tutorial2.png");
	private var _tutorial3:SharpImage = new SharpImage("img/tutorial3.png");
	private var _tutorial4:SharpImage = new SharpImage("img/tutorial4.png");
	private var _tutorialcoop:SharpImage = new SharpImage("img/coop.png");
	
	private var _difficultyTimer:Int = 0;
	
	static public var _tutorialProgress:Int = 0;
	static public var _coopProgress:Int = 0;
	
	public function new(players:Int)
	{
		super();
		Instance = this;
		
		_numPlayers = players;
		
		MainEmitter.newType("shotgundust", [0]);
		MainEmitter.setMotion("shotgundust", 0 - 45, 30, 10 / 60.0, 90, 50, 10 / 60.0);
		MainEmitter.setAlpha("shotgundust");
		MainEmitter.newType("shotgundustr", [0]);
		MainEmitter.setMotion("shotgundustr", 180 - 45, 30, 10 / 60.0, 90, 50, 10 / 60.0);
		MainEmitter.setAlpha("shotgundustr");
		MainEmitter.newType("greatshiny", [0]);
		MainEmitter.setMotion("greatshiny", 0, 30, 15 / 60.0, 360, 50, 15 / 60.0);
		MainEmitter.setAlpha("greatshiny");
		MainEmitter.newType("snaredust", [0]);
		MainEmitter.setMotion("snaredust", 90 - 45 / 2.0, 10, 10 / 60.0, 45, 10, 5 / 60.0);
		MainEmitter.setAlpha("snaredust");
		MainEmitter.newType("icehit", [0]);
		MainEmitter.setMotion("icehit", 0, 30, 15 / 60.0, 360, 50, 15 / 60.0);
		MainEmitter.setAlpha("icehit");
		MainEmitter.newType("bad", [1]);
		MainEmitter.setMotion("bad", 90, 10, 45 / 60.0, 0, 0, 0);
		MainEmitter.setAlpha("bad");
	}
	
	public function GreatShine(x:Float, y:Float)
	{
		for (i in 0...30) {
			MainEmitter.emit("greatshiny", x, y);
		}
	}
	
	public override function begin()
	{
		// Add level.
		_level = new Level(0);
		add(_level);
		
		// Spawn players.
		var player0:Player = new Player(200, 250, 0);
		add(player0);
		add(new PlayTracker(460, 40, 220));
		for (i in 0...8) {
			add(new EmptyMarker(460, 40 + 220 * i / 8.0, i / 2.0, player0));
			add(new BeatTracker(460, 40 + 220 * i / 8.0, i / 2.0, player0));
		}
		add(new WeaponIcon(460, 283, player0));
		add(new WeaponText(460, 283, player0));
		_players.push(player0);
		
		if (_numPlayers > 1) {
			var player1:Player = new Player(300, 250, 1);
			add(player1);
			add(new PlayTracker(40, 40, 220));
			for (i in 0...8) {
				add(new EmptyMarker(40, 40 + 220 * i / 8.0, i / 2.0, player1));
				add(new BeatTracker(40, 40 + 220 * i / 8.0, i / 2.0, player1));
			}
			add(new WeaponIcon(40, 283, player1));
			add(new WeaponText(40, 283, player1));
			_players.push(player1);
		}
		
		// Add particles.
		addGraphic(MainEmitter, -500);
		
		// Play music.
		CurrentSong = new Song();
		CurrentSong.Start();
		
		addGraphic(_p0ui, -1000);
		addGraphic(_p1ui, -1000);
		_p1ui.visible = _numPlayers > 1;
		
		// Game over.
		_gameOverText.size = 24;
		_gameOverText.originX = _gameOverText.textWidth / 2;
		_continueText.originX = _continueText.textWidth / 2;
		_continueText2.originX = _continueText2.textWidth / 2;
		_gameOverText.visible = false;
		_continueText.visible = false;
		_continueText2.visible = false;
		_scoreText.originX = _scoreText.textWidth / 2;
		
		_gameOverText.smooth = false;
		_continueText.smooth = false;
		_continueText2.smooth = false;
		_scoreText.smooth = false;
		
		addGraphic(_gameOverText, -5000);
		addGraphic(_continueText, -5000);
		addGraphic(_continueText2, -5000);
		addGraphic(_scoreText, -5000);
		
		var version:Text = new Text(Main.kVersion, 0, 0);
		version.size = 8;
		version.smooth = false;
		addGraphic(version, -5000);

		_tutorialfader.scale = 2000;
		_tutorialfader.color = 0x000000;
		//addGraphic(_tutorialfader, -6000);
		_tutorialfader.alpha = _tutorialProgress < 5 ? 0.5 : 0;
		
		_tutorial0.alpha = 0;
		_tutorial1.alpha = 0;
		_tutorial2.alpha = 0;
		_tutorial3.alpha = 0;
		_tutorial4.alpha = 0;
		_tutorialcoop.alpha = 0;
		addGraphic(_tutorial0, -7000);
		addGraphic(_tutorial1, -7000, 30, 20);
		addGraphic(_tutorial2, -7000, 48, 10);
		addGraphic(_tutorial3, -7000, 5);
		addGraphic(_tutorial4, -7000, 5);
		addGraphic(_tutorialcoop, -7000);
		
		_fader.scale = 2000;
		_fader.alpha = 0;
		_fader.color = 0x000000;
		addGraphic(_fader, -10000);
	}
	
	override public function update() 
	{
		super.update();
		
		#if debug
		if (Input.pressed(Key.BACKSPACE)) {
			var temp:Array<Entity> = new Array<Entity>();
			getClass(Item, temp);
			for (item in temp) {
				item.x = _players[0].x;
				item.y = _players[0].y;
			}
		}
		#end
		
		CurrentSong.Update();
		
		_scoreText.text = "Score: " + _score;
		if (_shake > 0) {
			HXP.camera.x = HXP.rand(2) * 2 - 1;
			HXP.camera.y = HXP.rand(2) * 2 - 1;
			--_shake;
		} else {
			HXP.camera.x = 0;
			HXP.camera.y = 0;
		}

		
		
		_tutorial0.alpha -= 0.1;
		_tutorial1.alpha -= 0.1;
		_tutorial2.alpha -= 0.1;
		_tutorial3.alpha -= 0.1;
		_tutorial4.alpha -= 0.1;
		_tutorialcoop.alpha -= 0.1;
		// tutorial stuff.
		if (_numPlayers == 1) {
			if (_tutorialProgress == 0) {
				_tutorial0.alpha += 0.2;
			}
			else if (_tutorialProgress == 1) {
				_tutorial1.alpha += 0.2;
			}
			else if (_tutorialProgress == 2) {
				_tutorial2.alpha += 0.2;
			}
			else if (_tutorialProgress == 3) {
				_tutorial3.alpha += 0.2;
			}
			else if (_tutorialProgress == 4) {
				_tutorial4.alpha += 0.2;
			}
			
			if (_tutorialProgress > 2) {
				_tutorialfader.x = Math.max( -100, _tutorialfader.x - 1);
			}
			
			
		
			if (_tutorialProgress < 5) {
				if (Input.pressed(Key.ENTER) || Input.pressed(Key.SPACE) || (_tutorialProgress == 1 && _players[0].y < 190)) {
					_tutorialProgress++;
					new Sfx("sfx/tutorial" + AudioExt.Ext).play();
					
					
					if (_tutorialProgress == 2) {
						var item:Item = create(Item);
						item.Reset(250, 140);
					}
				}
				
				
				return;
			} else {
				_tutorialfader.alpha -= 0.1;
			}			
		}
		
		
		if (_numPlayers > 1) {
			if (_coopProgress < 1) {
				_tutorialcoop.alpha += 0.2;

				if (Input.pressed(Key.ENTER) || Input.pressed(Key.SPACE)) {
					_coopProgress++;
					new Sfx("sfx/tutorial" + AudioExt.Ext).play();
				}
				
				return;
			}
			
		}


		
		
		
		
		
		if (_fadeTimer > 0) {
			// Just fade.
			_fader.alpha += 1 / kFadeoutDuration;
			CurrentSong.FadeUpdate(kFadeoutDuration);
			
			if (_fadeTimer > kFadeoutDuration) {
				CurrentSong.Stop();
				HXP.scene = new MenuScene();
			}
			_fadeTimer++;
			return;
		}
		
		
		
		_difficultyTimer++;
		
		var gameOver:Bool = true;
		var players:Array<Player> = new Array<Player>();
		getClass(Player, players);
		for (player in players) {
			if (!player.Dead) {
				gameOver = false;
			}
		}
		_gameOverText.visible = gameOver;
		_continueText.visible = gameOver;
		_continueText2.visible = gameOver;
		
		if (gameOver && Input.pressed(Key.ENTER)) {
			// reset
			ResetGame();
		} else if (gameOver && Input.pressed(Key.ESCAPE)) {
			// exit.
			new Sfx("sfx/tutorial" + AudioExt.Ext).play();
			_fadeTimer++;
		}

		if (!gameOver) {
			HandleSpawnEnemies();
		}
		
		var items:Array<Entity> = new Array<Entity>();
		getClass(Item, items);
		if (items.length == 0) {
			// generate an item
			GenerateItem();
		}
	}
	
	private function HandleSpawnEnemies()
	{
		var enemiearst:Array<Entity> = new Array<Entity>();
		getType("enemy", enemiearst);
		if (enemiearst.length > 40) {
			// Hard cap on number of enemies to prevent shenanigans.
			return;
		}
		
		_enemyCounter++;
		
		var inter:Int = kEnemySpawnInterval;
		if (_numPlayers > 1) {
			inter = kEnemySpawnIntervalCoop;
		}
		
		if (_score < 10) {
			inter += 30;
		}
		if (_score < 10) {
		}
		if (_score > 25) {
			inter -= 10;
		}
		if (_score > 50) {
			inter -= 10;
		}
		if (_score > 100) {
			inter -= 20;
		}
		if (_score > 200) {
			inter -= 20;
		}
		if (_score > 300) {
			inter -= 20;
		}
		
		if (_enemyCounter >= inter && _enemyCounter < 10000) {
			var r:Int = HXP.rand(8);
			switch (r) {
				case 0:
					_enemyCounter = 10000;
				case 1:
					_enemyCounter = 20000;
				case 2:
					_enemyCounter = 30000;
				case 3:
					_enemyCounter = 40000;
				case 4:
					_enemyCounter = 50000;
				case 5:
					_enemyCounter = 60000;
				case 6:
					_enemyCounter = 70000;
				case 7:
					_enemyCounter = 80000;
			}
		}
		
		if (_enemyCounter == 10000) {
			var enemy:MeleeEnemy = create(MeleeEnemy);
			enemy.Reset(92, 80, 1);
		}
		if (_enemyCounter == 10030) {
			var enemy:MeleeEnemy = create(MeleeEnemy);
			enemy.Reset(92, 80, 1);
		}
		if (_enemyCounter == 10060) {
			var enemy:MeleeEnemy = create(MeleeEnemy);
			enemy.Reset(92, 80, 1);
			_enemyCounter = 0;
		}

		if (_enemyCounter == 20000) {
			var enemy:MeleeEnemy = create(MeleeEnemy);
			enemy.Reset(408, 80, -1);
		}
		if (_enemyCounter == 20030) {
			var enemy:MeleeEnemy = create(MeleeEnemy);
			enemy.Reset(408, 80, -1);
		}
		if (_enemyCounter == 20060) {
			var enemy:MeleeEnemy = create(MeleeEnemy);
			enemy.Reset(408, 80, -1);
			_enemyCounter = 0;
		}

		if (_enemyCounter == 30000) {
			var enemy:RangedEnemy = create(RangedEnemy);
			enemy.Reset(110, 35);
			_enemyCounter = 0;
		}
		if (_enemyCounter == 40000) {
			var enemy:RangedEnemy = create(RangedEnemy);
			enemy.Reset(390, 35);
			_enemyCounter = 0;
		}
		if (_enemyCounter == 50000) {
			var enemy:RangedEnemy = create(RangedEnemy);
			enemy.Reset(70, 235);
			_enemyCounter = 0;
		}
		if (_enemyCounter == 60000) {
			var enemy:RangedEnemy = create(RangedEnemy);
			enemy.Reset(430, 235);
			_enemyCounter = 0;
		}

		if (_enemyCounter == 70000) {
			var enemy:StrongEnemy = create(StrongEnemy);
			enemy.Reset(92, 80, 1);
			_enemyCounter = 0;
		}
		if (_enemyCounter == 80000) {
			var enemy:StrongEnemy = create(StrongEnemy);
			enemy.Reset(408, 80, -1);
			_enemyCounter = 0;
		}
	}
	
	static var _itemXs:Array<Int> = [120, 380, 130, 370, 250, 180, 320, 210, 290, 250, 110, 390];
	static var _itemYs:Array<Int> = [260, 260, 180, 180, 140, 140, 140, 220, 220, 260, 260, 260];
	public function GenerateItem()
	{
		var item:Item = create(Item);
		item.Reset(0, 0);

		for (x in 0...50) {
			var i:Int = HXP.rand(_itemXs.length);
			item.x = _itemXs[i];
			item.y = _itemYs[i];
			
			var good:Bool = true;
			for (player in _players) {
				if (item.distanceFrom(player) <= 50) {
					good = false;
				}
			}
			if (!good) {
				continue;
			}
			
			break;
		}
	}
	
	public function Score()
	{
		_score++;
	}
	
	public function ResetGame()
	{
		_enemyCounter = 0;
		var delete:Array<Entity> = new Array<Entity>();
		getType("playerbullet", delete);
		getType("enemybullet", delete);
		getType("enemy", delete);
		getType("item", delete);
		for (entity in delete) {
			recycle(entity);
		}
		
		for (player in _players) {
			player.UnDie();
		}

		_players[0].x = 200;
		_players[0].y = 250;
		if (_numPlayers > 1) {
			_players[1].x = 300;
			_players[1].y = 250;
		}
		
		var flash:ScreenFlash = create(ScreenFlash);
		flash.Reset(0xFFFFFF, 1.0);
		
		new Sfx("sfx/gamestart" + AudioExt.Ext).play();
		
		_score = 0;
	}
	
	public function Shake(duration:Int)
	{
		_shake = duration;
	}
}
