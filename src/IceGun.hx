package;

import com.haxepunk.Graphic;
import com.haxepunk.graphics.Spritemap;
import com.haxepunk.HXP;
import com.haxepunk.Mask;
import com.haxepunk.Sfx;

/**
 * ...
 * @author DDRKirby(ISQ)
 */
class IceGun extends Weapon
{
	static inline var kFireX = 10;
	static inline var kFireY = 10;
	
	public function new()
	{
		super(new SharpSpritemap("img/icegun.png", 40, 40));
		layer = -10;
		type = "ice";
		
		ThisSpritemap.add("idle", [0], 30);
	}
	
	public function Reset()
	{
		super.BaseReset();
		Beats = [1, 1.5, 2.5, 3];
		ThisSpritemap.play("idle");
	}
	
	override public function update():Void 
	{
		super.update();
	}
	
	override public function FireGreat(direction:Int)
	{
		_recoil = 5;
		var fireX:Float = x + kFireX * direction;
		var fireY:Float = y + kFireY;
		super.FireGreat(direction);
		var bullet:IceBullet = MainScene.Instance.create(IceBullet);
		bullet.Reset(fireX, fireY, 4 * direction, HXP.random * 4 - 2, 4, 1, 120, direction);
		MainScene.Instance.Shake(2);
		new Sfx("sfx/ice" + AudioExt.Ext).play();
		MainScene.Instance.GreatShine(fireX, fireY);
		
	}		
	
	override public function FireGood(direction:Int) 
	{
		_recoil = 5;
		var fireX:Float = x + kFireX * direction;
		var fireY:Float = y + kFireY;
		super.FireGood(direction);
		var bullet:IceBullet = MainScene.Instance.create(IceBullet);
		bullet.Reset(fireX, fireY, 4 * direction, HXP.random * 4 - 2, 4, 1, 120, direction);
		new Sfx("sfx/ice" + AudioExt.Ext).play();
	}
}
