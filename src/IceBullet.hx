package;
import com.haxepunk.HXP;
import com.haxepunk.Sfx;

/**
 * ...
 * @author DDRKirby(ISQ)
 */
class IceBullet extends PlayerBullet
{
	private var _image:SharpImage = new SharpImage("img/icebullet.png");
	private var _timer:Int = 0;

	public function new() 
	{
		super();
		graphic = _image;
		setHitbox(12, 12, 6, 6);
		_image.centerOrigin();
	}
	
	public function Reset(x:Float, y:Float, velX:Float, velY:Float, damage:Float, hits:Int, duration:Int, direction:Int)
	{
		_timer = 0;
		super.BaseReset(x, y, velX, velY, damage, hits, duration);
	}
	
	override public function update():Void 
	{
		super.update();
		_timer++;
	}
	
	override function CollideWithEnemy(enemy:Enemy):Void 
	{
		super.CollideWithEnemy(enemy);
		
		new Sfx("sfx/icehit" + AudioExt.Ext).play();
		for (i in 0...15) {
			MainScene.Instance.MainEmitter.emit("icehit", x, y);
		}
	}
	
	override private function CollideWithLevel():Void
	{
		for (i in 0...5) {
			MainScene.Instance.MainEmitter.emit("icehit", x, y);
		}
		
		x -= _velX;
		y -= _velY;
		
		if (collide("level", x + _velX, y) != null) {
			_velX = -_velX;
		}
		if (collide("level", x, y + _velY) != null) {
			_velY = -_velY;
		}
		
		if (collide("level", x + _velX, y + _velY) != null) {
			// try again?
			x += _velX;
			y += _velY;
		}
		if (collide("level", x + _velX, y + _velY) != null) {
			// give up.
			super.CollideWithLevel();
		}
		
		
		//_velX = _velX > 0 ? -HXP.random * 4 : HXP.random * 4;
		//_velY = _velY > 0 ? -HXP.random * 4 : HXP.random * 4;
	}
}