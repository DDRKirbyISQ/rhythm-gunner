package;

import com.haxepunk.Graphic.TileType;
import com.haxepunk.graphics.Spritemap;

/**
 * ...
 * @author DDRKirby(ISQ)
 */
class SharpSpritemap extends Spritemap
{
	public function new(source:TileType, frameWidth:Int=0, frameHeight:Int=0, ?cbFunc:Void->Void) 
	{
		super(source, frameWidth, frameHeight, cbFunc);
		smooth = false;
	}
}