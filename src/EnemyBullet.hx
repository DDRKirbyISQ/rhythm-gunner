package;

import com.haxepunk.Entity;
import com.haxepunk.Graphic;
import com.haxepunk.HXP;
import com.haxepunk.Mask;

/**
 * ...
 * @author DDRKirby(ISQ)
 */
class EnemyBullet extends Entity
{
	private var _velX:Float;
	private var _velY:Float;
	private var _duration:Int;

	public function new()
	{
		super();
		type = "enemybullet";
		layer = -100;
	}
	
	public function BaseReset(x:Float, y:Float, velX:Float, velY:Float, duration:Int)
	{
		this.x = x;
		this.y = y;
		_velX = velX;
		_velY = velY;
		_duration = duration;
	}
	
	override public function update():Void 
	{
		super.update();
		
		Move();
		
		HandleDuration();
		
		if (collide("level", x, y) != null) {
			CollideWithLevel();
		}
		
		var collidedPlayer:Entity = collide("player", x, y);
		if (collidedPlayer != null) {
			CollideWithPlayer(cast(collidedPlayer, Player));
		}
	}
	
	private function HandleDuration()
	{
		_duration--;
		if (_duration == 0) {
			MainScene.Instance.recycle(this);
		}
	}
	
	private function Move()
	{
		x += _velX;
		y += _velY;
	}
	
	private function CollideWithLevel():Void
	{
		MainScene.Instance.recycle(this);
	}
	
	private function CollideWithPlayer(player:Player):Void
	{
		player.Hit();
		MainScene.Instance.recycle(this);
	}
}