import com.haxepunk.Engine;
import com.haxepunk.HXP;
import com.haxepunk.utils.Input;
import com.haxepunk.utils.Key;

class Main extends Engine
{
	public static inline var kVersion:String = "v1.04";

	override public function init()
	{
		HXP.randomizeSeed();
		
		#if debug
		HXP.console.enable();
		#end
		
		HXP.screen.scale = 2;
		HXP.screen.smoothing = false;
		//HXP.scene = new MainScene(1);
		HXP.scene = new MenuScene();
		
		Input.define("player1_jump", [Key.S, Key.R, Key.W]);
		Input.define("player1_left", [Key.Z, Key.A]);
		Input.define("player1_right", [Key.C, Key.D]);
		Input.define("player1_shoot", [Key.SPACE, Key.V, Key.F, Key.J, Key.B]);
		Input.define("player0_jump", [Key.UP]);
		Input.define("player0_left", [Key.LEFT]);
		Input.define("player0_right", [Key.RIGHT]);
		Input.define("player0_shoot", [Key.CONTROL, Key.SHIFT, Key.NUMPAD_0]);
	}

	public static function main() { new Main(1000, 600, 60, true); }
}
