package;

import com.haxepunk.Entity;
import com.haxepunk.Graphic;
import com.haxepunk.graphics.Image;
import com.haxepunk.Mask;
import com.haxepunk.masks.Grid;
import com.haxepunk.masks.Imagemask;
import com.haxepunk.masks.Pixelmask;
import openfl.display.BitmapData;
import openfl.geom.Point;

/**
 * ...
 * @author DDRKirby(ISQ)
 */
class Level extends Entity
{
	private static inline var kAscii:String =
"XXXXXXXXXXXXXXXXXXXXXXXXX\n" +
"XXXXXXXXXXXXXXXXXXXXXXXXX\n" +
"XXXXXXXXXXXXXXXXXXXXXXXXX\n" +
"XXXXXXXXXXXXXXXXXXXXXXXXX\n" +

"XXXX                 XXXX\n" +
"XXXX                 XXXX\n" +
"XXXX                 XXXX\n" +
"XXXX                 XXXX\n" +
"XXXXXXX           XXXXXXX\n" +
"XXXX                 XXXX\n" +

"XXXX                 XXXX\n" +

"XXXX                 XXXX\n" +
"XXXX                 XXXX\n" +
"XXXX                 XXXX\n" +

"XXXX    XXXXXXXXX    XXXX\n" +

"XXXX                 XXXX\n" +
"XXXX                 XXXX\n" +
"XXXX                 XXXX\n" +

"XXXXXXXXX       XXXXXXXXX\n" +

"XXXX                 XXXX\n" +
"XXXX                 XXXX\n" +
"XXXX                 XXXX\n" +

"XXXX     XX   XX     XXXX\n" +

"XXXX                 XXXX\n" +
"XXXX                 XXXX\n" +
"XXXX                 XXXX\n" +

"XXXXXXXXXXXXXXXXXXXXXXXXX\n" +
"XXXXXXXXXXXXXXXXXXXXXXXXX\n" +
"XXXXXXXXXXXXXXXXXXXXXXXXX\n" +
"XXXXXXXXXXXXXXXXXXXXXXXXX\n";

	public function new(levelNumber:Int)
	{
		var image:SharpImage = new SharpImage('img/level${levelNumber}.png');
		var grid:Grid = new Grid(500, 300, 20, 10);
		var rows:Array<String> = kAscii.split('\n');
		
		for (y in 0...rows.length) {
			for (x in 0...rows[y].length ) {
				if (rows[y].charAt(x) == 'X') {
					grid.setTile(x, y);
				}
			}
		}
		super(0, 0, image, grid);
		type = "level";
		layer = 100;
	}
}
