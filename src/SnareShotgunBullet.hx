package;

/**
 * ...
 * @author DDRKirby(ISQ)
 */
class SnareShotgunBullet extends PlayerBullet
{
	private var _spritemap:SharpSpritemap = new SharpSpritemap("img/shotgun_bullet.png", 80, 40);
	private var _timer:Int = 0;

	public function new() 
	{
		super();
		graphic = _spritemap;
		setHitbox(80, 20, 0, 13);
		_spritemap.originY = 18;
		_spritemap.add("fire", [0, 1, 2, 3, 4, 5], 20);
	}
	
	public function Reset(x:Float, y:Float, velX:Float, velY:Float, damage:Float, hits:Int, duration:Int, direction:Int)
	{
		_spritemap.alpha = 1;
		_timer = 0;
		super.BaseReset(x, y, velX, velY, damage, hits, duration);
		_spritemap.flipped = direction == -1;
		_spritemap.updateBuffer();
		_spritemap.originX = direction == -1 ? _spritemap.width : 0;
		originX = Math.round(_spritemap.originX);
		_spritemap.play("fire", true);
		
		for (i in 0...30) {
			MainScene.Instance.MainEmitter.emit(direction == -1 ? "shotgundustr" : "shotgundust", x, y);
		}
	}
	
	override public function update():Void 
	{
		super.update();
		_timer++;
		if (_timer > 9) {
			_spritemap.alpha -= 0.1;
		}
	}
	
	override function CollideWithLevel():Void 
	{
		
	}
}