package;

import com.haxepunk.Entity;
import com.haxepunk.Graphic;
import com.haxepunk.graphics.Spritemap;
import com.haxepunk.Mask;
import com.haxepunk.Sfx;

/**
 * ...
 * @author DDRKirby(ISQ)
 */
class Weapon extends Entity
{
	public var ThisSpritemap:Spritemap;
	private var _recoil:Int = 0;
	
	public static inline var kGoodThreshold:Float = 0.25;
	public static inline var kGreatThreshold:Float = 0.1;
	public static inline var kPoorThreshold:Float = 0.5;
	
	public var AlreadyUsedBeats:Array<Float>;
	public var Beats:Array<Float>;
	public var PoorBeats:Array<Float>;
	
	private var _lastBeatPosition:Float = 0.0;
	
	private var _badSfx:Sfx = new Sfx("sfx/bad" + AudioExt.Ext);
	
	public function new(spritemap:Spritemap)
	{
		super(0, 0, spritemap);
		ThisSpritemap = spritemap;
	}
	
	public function BaseReset()
	{
		AlreadyUsedBeats = new Array<Float>();
		PoorBeats = new Array<Float>();
	}
	
	public function UpdateOrigin()
	{
		ThisSpritemap.originX = ThisSpritemap.flipped ? ThisSpritemap.width - _recoil : _recoil;
		ThisSpritemap.updateBuffer();
		if (_recoil > 1) {
			_recoil--;
		}
	}
	
	override public function update():Void 
	{
		super.update();
		
		if (_lastBeatPosition > MainScene.Instance.CurrentSong.BeatPosition()) {
			// we wrapped around, so reset.
			if (AlreadyUsedBeats.indexOf(4.0) == -1) {
				AlreadyUsedBeats = new Array<Float>();
			} else {
				AlreadyUsedBeats = new Array<Float>();
				AlreadyUsedBeats.push(0.0);
			}

			if (PoorBeats.indexOf(4.0) == -1) {
				PoorBeats = new Array<Float>();
			} else {
				PoorBeats = new Array<Float>();
				PoorBeats.push(0.0);
			}
		}
		_lastBeatPosition = MainScene.Instance.CurrentSong.BeatPosition();
	}
	
	public function TryFire(direction:Int)
	{
		Fire(direction);
	}
	
	public function Fire(direction:Int)
	{
		var tempArray:Array<Float> = new Array<Float>();
		for (beat in Beats) {
			if (AlreadyUsedBeats.indexOf(beat) == -1) {
				tempArray.push(beat);
			}
		}
		var result: { result:Float, beatResult:Float } = MainScene.Instance.CurrentSong.BeatDiff(tempArray);
		var beatDiff = result.result;
		var usedBeat = result.beatResult;
		
		if (beatDiff < Weapon.kGreatThreshold) {
			FireGreat(direction);
			AlreadyUsedBeats.push(usedBeat);
		} else if (beatDiff < Weapon.kGoodThreshold) {
			FireGood(direction);
			AlreadyUsedBeats.push(usedBeat);
		} else if (beatDiff < Weapon.kPoorThreshold) {
			FirePoor(direction);
			AlreadyUsedBeats.push(usedBeat);
			PoorBeats.push(usedBeat);
		} else {
			MainScene.Instance.MainEmitter.emit("bad", x + direction * 10, y);

			_badSfx.play(1.5);
		}
	}
	
	public function FireGreat(direction:Int)
	{
	}
	
	public function FireGood(direction:Int)
	{
	}
	
	public function FirePoor(direction:Int)
	{
		MainScene.Instance.MainEmitter.emit("bad", x + direction * 10, y);
		
		_badSfx.play(1.5);
	}
}
