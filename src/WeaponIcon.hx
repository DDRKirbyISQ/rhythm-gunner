package;
import com.haxepunk.Entity;
import com.haxepunk.graphics.Image;

/**
 * ...
 * @author DDRKirby(ISQ)
 */
class WeaponIcon extends Entity
{
	private var _player:Player;
	private var _snareImage:SharpImage = new SharpImage("img/shotgun_item.png");
	private var _bassgunImage:SharpImage = new SharpImage("img/bassgun_item.png");
	private var _lasergunImage:SharpImage = new SharpImage("img/lasergun_item.png");
	private var _icegunImage:SharpImage = new SharpImage("img/icegun_item.png");

	public function new(x:Float, y:Float, player:Player)
	{
		_player = player;
		UpdateImage();
		_snareImage.originX = _snareImage.width / 2;
		_snareImage.originY = _snareImage.height;
		_bassgunImage.originX = _snareImage.width / 2;
		_bassgunImage.originY = _snareImage.height;
		_lasergunImage.originX = _snareImage.width / 2;
		_lasergunImage.originY = _snareImage.height;
		_icegunImage.originX = _snareImage.width / 2;
		_icegunImage.originY = _snareImage.height;
		super(x, y);
		addGraphic(_snareImage);
		addGraphic(_bassgunImage);
		addGraphic(_lasergunImage);
		addGraphic(_icegunImage);
	}
	
	override public function update():Void 
	{
		super.update();
		
		UpdateImage();
	}
	
	private function UpdateImage()
	{
		_snareImage.visible = Std.is(_player.CurrentWeapon, SnareShotgun);
		_bassgunImage.visible = Std.is(_player.CurrentWeapon, BassGun);
		_lasergunImage.visible = Std.is(_player.CurrentWeapon, LaserGun);
		_icegunImage.visible = Std.is(_player.CurrentWeapon, IceGun);
	}
}
