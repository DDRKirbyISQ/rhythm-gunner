package;

import com.haxepunk.Graphic;
import com.haxepunk.graphics.Spritemap;
import com.haxepunk.HXP;
import com.haxepunk.Mask;
import com.haxepunk.Sfx;

/**
 * ...
 * @author DDRKirby(ISQ)
 */
class BassGun extends Weapon
{
	static inline var kFireX = 15;
	static inline var kFireY = 16;
	
	public function new()
	{
		super(new SharpSpritemap("img/bassgun.png", 40, 40));
		layer = -10;
		type = "bass";
		
		ThisSpritemap.add("idle", [0, 1, 0, 2], 10);
	}
	
	public function Reset()
	{
		super.BaseReset();
		Beats = [0, 1, 2, 3, 4];
		ThisSpritemap.play("idle");
	}
	
	override public function update():Void 
	{
		super.update();
	}
	
	override public function FireGreat(direction:Int)
	{
		_recoil = 5;
		var fireX:Float = x + kFireX * direction;
		var fireY:Float = y + kFireY;
		super.FireGreat(direction);
		var bullet:BassGunBullet = MainScene.Instance.create(BassGunBullet);
		bullet.Reset(fireX, fireY, 4 * direction, 0, 2, 1, 60, direction);
		MainScene.Instance.Shake(2);
		new Sfx("sfx/bass" + AudioExt.Ext).play(1.2);
		MainScene.Instance.GreatShine(fireX, fireY);
		
	}		
	
	override public function FireGood(direction:Int) 
	{
		_recoil = 5;
		var fireX:Float = x + kFireX * direction;
		var fireY:Float = y + kFireY;
		super.FireGood(direction);
		var bullet:BassGunBullet= MainScene.Instance.create(BassGunBullet);
		bullet.Reset(fireX, fireY, 4 * direction, 0, 2, 1, 60, direction);
		new Sfx("sfx/bass" + AudioExt.Ext).play(1.2);
	}
}
