package;
import com.haxepunk.Sfx;

/**
 * ...
 * @author DDRKirby(ISQ)
 */
class BassGunBullet extends PlayerBullet
{
	private var _spritemap:SharpSpritemap = new SharpSpritemap("img/bassgun.png", 40, 40);
	private var _timer:Int = 0;

	public function new() 
	{
		super();
		graphic = _spritemap;
		setHitbox(27, 12, 0, 8);
		_spritemap.originY = 18;
		_spritemap.add("fire", [0, 1, 0, 2], 20);
	}
	
	public function Reset(x:Float, y:Float, velX:Float, velY:Float, damage:Float, hits:Int, duration:Int, direction:Int)
	{
		_timer = 0;
		super.BaseReset(x, y, velX, velY, damage, hits, duration);
		_spritemap.flipped = direction == -1;
		_spritemap.updateBuffer();
		_spritemap.originX = direction == -1 ? _spritemap.width:0;
		originX = Math.round(direction == -1 ? width : 0);
		_spritemap.play("fire", true);
	}
	
	override public function update():Void 
	{
		super.update();
		_timer++;
	}
	
	override private function CollideWithLevel():Void
	{
		new Sfx("sfx/bassgun_explode" + AudioExt.Ext).play(0.7);
		
		var explosion:Explosion = MainScene.Instance.create(Explosion);
		explosion.Reset(x + _velX * 4, y, 0.25, 20);
		
		super.CollideWithLevel();
	}
	
	override private function CollideWithEnemy(enemy:Enemy):Void
	{
		new Sfx("sfx/bassgun_explode" + AudioExt.Ext).play(0.7);

		var explosion:Explosion = MainScene.Instance.create(Explosion);
		explosion.Reset(x + _velX * 4, y, 0.25, 20);

		super.CollideWithEnemy(enemy);
	}
	
}