package;

import com.haxepunk.Entity;
import com.haxepunk.Graphic;
import com.haxepunk.graphics.Image;
import com.haxepunk.Mask;

/**
 * ...
 * @author DDRKirby(ISQ)
 */
class BeatCircle extends Entity
{
	static inline var kDurationInBeats:Float = 0.75;
	static inline var kMinScale:Float = 0.1;

	var _beat:Float;
	var _player:Player;
	var _image:SharpImage = new SharpImage("img/beatcircle.png");

	public function new()
	{
		super();
		_image.centerOrigin();
		graphic = _image;
		layer = 25;
	}

	public function Reset(player:Player, beat:Float)
	{
		_player = player;
		_beat = beat;
		x = player.x;
		y = player.y;
		_image.alpha = 0;
	}

	override public function update():Void 
	{
		super.update();
		
		var beatDiff:Float = _beat - MainScene.Instance.CurrentSong.BeatPosition();
		while (beatDiff < 0) {
			beatDiff += 4.0;
		}

		if (beatDiff > kDurationInBeats) {
			_image.alpha = 0;
			return;
		}

		var progress:Float = 1.0 - beatDiff / kDurationInBeats;
		_image.alpha = progress * 1.5;
		_image.scale = 1 - (1 - kMinScale) * progress;
	}
}
